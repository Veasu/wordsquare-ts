import {Trie} from '../src/trie'
const fs = require('fs');

let trie: Trie;
let textByLine: string[] = []

function init()
{
    const text = fs.readFileSync("./words.txt", "utf-8");
    textByLine = text.split("\n")
    trie = new Trie()
    trie.insertWords(textByLine, 0);
}

beforeAll(() => {
    return init();
});

test('Find All Possible Words - Length 3', () => {
    trie.changeSolutionLength(3);
    const foundWords = trie.getAllWords("aaabcccnr", 3)
    const possibleWords = [
        'aba', 'ana', 'arb',
        'arc', 'baa', 'ban',
        'bar', 'bra', 'cab',
        'can', 'car', 'nab',
        'ran']
    expect(foundWords).toEqual(possibleWords);
})

test('Find All Words From Prefix - Length 3', () => {
    trie.changeSolutionLength(3);
    const foundWords = trie.findWord("c", "aaabnr", 3, trie.getPrefixNode("c"))
    const possibleWords = [ 'cab', 'can', 'car' ]
    expect(foundWords).toEqual(possibleWords);
})

test('Find All Words From Prefix - No Solution', () => {
    trie.changeSolutionLength(3);
    const foundWords = trie.findWord("", "jsd", 3)
    expect(foundWords).toHaveLength(0);
})

test('Check Words From Prefix - Successful', () => {
    expect(trie.checkWordFromPrefix("bu")).toBeTruthy();
})

test('Check Words From Prefix - Failed', () => {
    expect(trie.checkWordFromPrefix("pp")).toBeFalsy();
})