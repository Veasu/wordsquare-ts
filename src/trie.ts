const {performance} = require('perf_hooks')
import {TrieNode} from './trie-node'

export class Trie
{
    /**
     * @param solutionLength The length of the word needed to find.
     */
    constructor(solutionLength: number = 4)
    {
        this.rootNode = new TrieNode();
        this.solutionLength = solutionLength
    }

    private rootNode: TrieNode;
    private solutionLength: number = 0

    /**
     * Inserts words into the trie, going through the words characters and adding nodes where needed and marking leafs.
     * @param wordsList Array of strings containing the words to add to the Trie
     * @param length Length of words to add. Will default to solution length, use 0 to add all words.
     */
    public insertWords(wordsList: string[], length: number = this.solutionLength): void
    {
        wordsList.forEach((word) => {
            let len = word.length
            if (len == length || length == 0)
            {
                let currentNode = this.rootNode
                for(let char of word)
                {
                    if(!currentNode.children[char])
                    {
                        currentNode.children[char] = new TrieNode()
                    }
                    currentNode = currentNode.children[char]
                }
                currentNode.isLeaf = true
            }
        })
    }
    
    /**
     * Get all words that can be made from the current letters string.
     * @param letters String of letters to use to create words
     * @param lenghtOfWords The length of word to find.
     * @returns Array of strings containing the words that have been found
     */
    public getAllWords(letters: string, lenghtOfWords: number): string[]
    {
        let listOfWords: string[] = []

        let uniqueLetters = Array.from(new Set(letters.split(""))).join("") // Get the unique letters in the string to stop wasted searches.

        // Loop through the unique letters of the string and use the full list letters minus that single letter to find words.
        for(let i = 0; i < uniqueLetters.length; i++)
        {
            let currentLetter = uniqueLetters.charAt(i)
            let remainingLetters = letters.replace(currentLetter, "")
            listOfWords.push(...this.findWord(currentLetter, remainingLetters, lenghtOfWords, this.rootNode.children[currentLetter]))
        }
        return listOfWords
    }
    
    /**
     * Iterates through the prefix seeing if there are any nodes at the end to see if this words can be used.
     * @param pre String containing the first x amount of letters for a word.
     */
    public checkWordFromPrefix(pre: string): boolean
    {
        let currentNode = this.rootNode
        for(let char of pre)
        {
            // Check if there are any childen for the current letter of the prefix.
            // If there is continue down the trie
            if (currentNode.children[char])
            {
                currentNode = currentNode.children[char]
            }
            else 
            {
                return false
            }

            if (!currentNode.children)
            {
                return false
            }
        }
        return true
    }

    /**
     * Gets the final node of the prefix string passed in.
     * @param prefix The prefix of the current word.
     */
    public getPrefixNode(prefix: string): TrieNode
    {
        let startingNode = this.rootNode
        for(let char of prefix)
        {
            if (startingNode.children[char])
            {
                startingNode = startingNode.children[char]
            }
        }
        return startingNode
    }

    /**
     * Sets the solution length of the trie used for finding words.
     * @param wordLength Length of word the trie should solve for.
     */
    public changeSolutionLength(wordLength: number): void
    {
        this.solutionLength = wordLength
    }

    /**
     * Clears the current nodes.
     */
    public clearNodes(): void
    {
        this.rootNode = new TrieNode()
    }

    /**
     * Finds words in the trie through recursion. Can be called from any node and returns an array of strings containing found words.
     * @param prefix 
     * @param remainingLetters 
     * @param lengthOfWord 
     * @param parentNode 
     * @param listOfWords 
     */
    public findWord(prefix: string, remainingLetters: string, lengthOfWord: number, parentNode: TrieNode = this.rootNode, listOfWords: string[] = []): string[]
    {
        let completedLetters: string[] = [] //Keep an array of completed letters to stop duplicate searches reducing time taken to find all words.
        for(let i = 0; i < remainingLetters.length; ++i)
        {
            let currentChar = remainingLetters.charAt(i)
            if(parentNode.children[currentChar] && !completedLetters.includes(currentChar)) // Check if letter has been checked for this iteration and that there are nodes available.
            {
                completedLetters.push(currentChar)
                let nextNode = parentNode.children[currentChar];
                let tempPrefix = prefix + currentChar
                if(tempPrefix.length == lengthOfWord && nextNode.isLeaf) // Check if we have found a usable word by checking length and if the node is marked as a leaf.
                {
                    listOfWords.push(tempPrefix)
                }
                if(tempPrefix.length < lengthOfWord) // Check if another iteration will go over the correct length of word. If not, call ourself again to find next letter.
                {
                    let tempRemaining = remainingLetters.replace(currentChar, '')
                    listOfWords = this.findWord(tempPrefix, tempRemaining, lengthOfWord, nextNode, listOfWords)
                }
            }
        }
        return listOfWords
    }
}