export class TrieNode 
{
    constructor(){
        this.children = {}
        this.isLeaf = false
    }
    children: { [id: string] : TrieNode; } // Array of nodes, indexed by a string. Can be used as a dictionary.
    isLeaf: boolean
}

