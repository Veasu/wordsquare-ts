const fs = require('fs')
const readline = require('readline-sync')
import {WordSolver} from './solver'

// GENERAL NOTES
// readline-sync is used as by default readline as an asynchronous process. This small npm package provides a synchronous version of this 
// Application can either be used with zero arguments passed in which will then prompt the user for the inputs, or with the two inputs passed when starting the application.

// Needed slice, first two arguments are paths to node and the application itself.
const myArgs = process.argv.slice(2)
const text = fs.readFileSync("./words.txt", "utf-8")
const textByLine = text.split("\n")
let characters = ""
let wordLength = 0
let solved = false

// Check if arguments have been passed
if(myArgs.length == 2)
{
    characters = myArgs[0]
    wordLength = Number.parseInt(myArgs[1])

    // Test the arguments are what we expect and if so pass to the solver.
    if (!/^[A-Za-z]+$/.test(characters))
    {
        console.log("Please Make Sure The Character String Contains Only Letters")
        readline.question("Please Press Any Key...")
    }
    else if (!Number.isInteger(wordLength))
    {
        console.log("Please Make Sure The Length Is A Single Number")
        readline.question("Please Press Any Key...")
    }
    else 
    {
        try
        {
            const solution = new WordSolver(wordLength, textByLine)
            const result = solution.printWordSquare(characters)
            for(const word of result)
            {
                console.log(word)
            }
        }
        catch(e)
        {
            console.log(e)
        }
    }
}
else if (myArgs.length == 0)
{
    // Loop to get inputs from user, ends once a square has been found.
    while (!solved)
    {
        console.clear();
        if (characters == "")
        {
            characters = readline.question("Please Input Character String: ")
        }

        // Testing input is just characters, if not show error and ask again.
        if (/^[A-Za-z]+$/.test(characters))
        {
            if (wordLength == 0)
            {
                wordLength = Number.parseInt(readline.question("Please Input Word Length: "))
            }

            // Testing input is numeric, if not show error and ask again
            if(Number.isInteger(wordLength))
            {
                try
                {
                    const solution = new WordSolver(wordLength, textByLine)
                    const result = solution.printWordSquare(characters)
                    for(const word of result)
                    {
                        console.log(word)
                    }
                }
                catch(e)
                {
                    console.log(e)
                    readline.question("Please Press Any Key...")
                }

                wordLength = 0;
                characters = "";
            }
            else
            {
                wordLength = 0;
                console.log("Please Make Sure The Length Is A Single Number")
                readline.question("Please Press Any Key...")
            }
        }
        else
        {
            characters = ""
            console.log("Please Make Sure The Character String Contains Only Letters")
            readline.question("Please Press Any Key...")
        }
    }
}
else
{
    console.log("Please either pass in 2 arugments in the form of charcter string and then length of word, or pass zero arguments and input them when asked.")
}
