# Wordsquare Solver (TS)

Live Demo: [Word Square Solver](https://wordsquare.veasu.com/)

Small wordsquare solver written in Typescript. Uses a Trie structure that was created by myself instead of a premade package

## Prerequisites

Node.js

NPM

Typescript (For Rebuilding Only)

## Installation

Open the base folder in a terminal and install needed packages using NPM

```bash
npm install
```

Once installed in the same location run

```bash
npm run build
```

to rebuild the application to the /bin directory if wanted or use the prebuild files with the command below.

## Usage

The application can be called from the command line form the base directory using

```bash
npm run solve (characters string here) (word length here)
```

or by opening the bin folder and running 

```bash
node .\index.js (characters string here) (word length here)
```

The application can be called with no arguments too. This will cause the application to prompt the user for the inputs at runtime instead.

## Results

Results are printed to the terminal used to call the application in the form 

```bash
Found 7x7 square in: 660.5410999655724 ms / 0.6605410999655723 seconds
bravado
renamed
analogy
valuers
amoebas
degrade
odyssey
```

## Tests

All tests can be ran by using the command 

```bash
npm run test
```

## License
[MIT](https://choosealicense.com/licenses/mit/)