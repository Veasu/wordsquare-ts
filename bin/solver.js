"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WordSolver = void 0;
const trie_1 = require("./trie");
const { performance } = require('perf_hooks');
class WordSolver {
    /**
     * @param wordLength The length of the word needed to find.
     * @param dictionary String array containing the words to add to the Trie.
     */
    constructor(wordLength, dictionary) {
        /*
         * Private Vars
         */
        this.requiredSolveLength = 0;
        this.requiredSolveLength = wordLength;
        this.trieStructure = new trie_1.Trie(this.requiredSolveLength);
        this.trieStructure.insertWords(dictionary, this.requiredSolveLength);
    }
    /*
     * Public Functions
     */
    /**
     * Takes the letter string as an input and starts the recursive operation using the internal Trie structure to find the word sqaure for the given length.
     * Returns array if found, throw an eror if not.
     * @param letterString The length of the word needed to find.
     * @returns An array of words if found
     */
    printWordSquare(letterString) {
        if (letterString.length % this.requiredSolveLength !== 0) {
            throw new Error("Please make the number of letters and the size of the grid a divisible number");
        }
        else {
            let t0 = performance.now();
            let t1 = 0;
            // Find all words that can be made from the string. Use this to start the square with and iterate through if word is found to not be solvable.
            // TO-DO Check if it is faster to not find inital word list.
            let wordsList = this.trieStructure.getAllWords(letterString, this.requiredSolveLength);
            let wordSquare = [];
            for (let word of wordsList) {
                wordSquare = this.createSquare(word, letterString);
                if (wordSquare.length == this.requiredSolveLength) {
                    t1 = performance.now();
                    break;
                }
            }
            // Print out the word square if found.
            if (wordSquare.length == this.requiredSolveLength) {
                console.log(`Found ${this.requiredSolveLength}x${this.requiredSolveLength} square in: ${(t1 - t0)} ms / ${(t1 - t0) / 1000} seconds`);
                return wordSquare;
            }
            else {
                throw new Error("Failed To Solve.");
            }
        }
    }
    /**
     * Clears the current dictionary.
     */
    clearDictionary() {
        this.trieStructure.clearNodes();
    }
    /**
     * Change the needed word length for a successful solve.
     * @param wordLength The length of the word needed to find.
     */
    changeWordLength(wordLength) {
        this.requiredSolveLength = wordLength;
    }
    /**
     * Change the current dictionary of nodes.
     * @param wordList The list of words to insert into the Trie
     */
    changeDictionary(wordList) {
        this.clearDictionary();
        this.trieStructure.insertWords(wordList, this.requiredSolveLength);
    }
    /**
     * Change both the dictionary and the length of word needed to find.
     * @param wordList The list of words to insert into the Trie
     * @param wordLength The length of the word needed to find.
     */
    changeWordLengthAndDictionary(wordList, wordLength) {
        this.clearDictionary();
        this.requiredSolveLength = wordLength;
        this.trieStructure.insertWords(wordList, this.requiredSolveLength);
    }
    /*
     * Private Functions
     */
    /**
     * Internal function used by the wordSolver to start the solving process.
     * Takes the first word from the inital list and passes it as an array, and with the inital row number as 1 to the solveSquare method.
     * Returns an array of strings that are it's current solution. Can be either completed or unsuccessful attempts.
     * @param word The starting word for the current square.
     * @param letters String that contains the letters to solve the square
     * @returns Array of strings containing the solution - complete or failed.
     */
    createSquare(word, letters) {
        let candidateWords = [];
        candidateWords.push(word);
        for (const char of word) // Iterates through the word and removed the used letters from the letters string
         {
            letters = letters.replace(char, "");
        }
        return this.solveSquare(candidateWords, letters, 1);
    }
    /**
     * Main internal function that is recursively called to solve the square. Takes the current words in the solution, finds possible words for the next row and then tests if they fit
     * the current solution. If it does recursively calls itself with the found word added to the array to continue the solving process, if not returns the current solution so that another word can be tried for the
     * previous row until a solution is found or the entire solution is found to be unsolvable with the current starting word.
     * @param word String array which contains the current solution
     * @param remainingLetters String that contains the remaining letters of the string passed in to solve
     * @param row The current row that is trying to be solved (indexed from 1 not 0)
     * @returns Array containing the current solution
     */
    solveSquare(word, remainingLetters, row) {
        // Find all possible words for this row, if it finds a word that fits and is the last row, returns the word array to be printed.
        let possible_words = this.findWordsForRow(word, remainingLetters, row);
        if (row == this.requiredSolveLength - 1 && possible_words[0]) {
            word.push(possible_words[0]);
            return word;
        }
        else if (!possible_words) {
            return word;
        }
        else {
            // If the word wouldn't complete the square check all possible words by seeing if there are words that could be found for the next row 
            // if they were inserted into the current word square by using the prefixes.
            for (let possWord of possible_words) {
                let tempWords = [...word];
                tempWords.push(possWord);
                let tempLetters = remainingLetters;
                if (this.checkWordForRow(tempWords, row)) {
                    // Remove the used characters from the remaining letters and reccursively call the function to find the next row.
                    // Return if the square returned is finished, else move onto the next word for this row.
                    Array.from(possWord).forEach((char) => tempLetters = tempLetters.replace(char, ""));
                    let square = this.solveSquare(tempWords, tempLetters, row + 1);
                    if (square.length == this.requiredSolveLength) {
                        return square;
                    }
                }
            }
        }
        return word;
    }
    /**
     * Checks to see if the current words in the array have a solution for the next row. This is done by reading characters from top to bottom in the current square for each row of the sqaure.
     * These prefixes are then searched to see if they have usable words, if not we exit early else we continue with the current square.
     * @param words Array of strings containing the words of the current square solution
     * @param currentRow Positive integer of which row is being solved.
     */
    checkWordForRow(words, currentRow) {
        let prefixes = [];
        let prefix = "";
        // Read needed prefixed from top to bottom in the current square.
        for (let i = currentRow; i < this.requiredSolveLength; i++) {
            prefix = "";
            words.forEach((word) => prefix += word.charAt(i)); // For each column read down each word in that position and add to the prefix.
            prefixes.push(prefix);
        }
        // Go through each prefix and check they have words that can be used.
        for (let pre of prefixes) {
            if (!this.trieStructure.checkWordFromPrefix(pre)) {
                return false;
            }
        }
        return true;
    }
    /**
     * Takes the current word square, finds the prefixes from reading the characters from top to bottom in each row.
     * Uses these prefixes to find a list of usable words for the next row and returns them.
     * @param words Array of strings containing the words of the current square solution
     * @param remainingLetters String of remaining letters that can be used to solve the square
     * @param row Positive integer of which row is being solved.
     */
    findWordsForRow(words, remainingLetters, row) {
        let prefix = "";
        words.forEach((word) => {
            prefix += word.charAt(row);
        });
        // Go through the prefix characters and check if they are present in the remaining letters string.
        // If not would not be possible to solve square so exit early and return no new words.
        let tempLetters = remainingLetters;
        for (let i = 0; i < prefix.length; i++) {
            if (!remainingLetters.includes(prefix.charAt(i))) {
                return [];
            }
            tempLetters.replace(prefix.charAt(i), "");
        }
        // Remove the letters used in the prefix from the remaining letters
        Array.from(prefix).forEach((char) => remainingLetters = remainingLetters.replace(char, ""));
        // Check if the prefix has any words, if not return early.
        if (!this.trieStructure.checkWordFromPrefix(prefix)) {
            return [];
        }
        // Use the Trie to find all the words that can be used for the next row using the found prefix and the remaining letters. Return a list of words.
        let res = this.trieStructure.findWord(prefix, remainingLetters, this.requiredSolveLength, this.trieStructure.getPrefixNode(prefix));
        return res;
    }
}
exports.WordSolver = WordSolver;
//# sourceMappingURL=solver.js.map