"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrieNode = void 0;
class TrieNode {
    constructor() {
        this.children = {};
        this.isLeaf = false;
    }
}
exports.TrieNode = TrieNode;
//# sourceMappingURL=trie-node.js.map